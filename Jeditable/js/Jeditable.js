 $(document).ready(function() {
    $('[data-jeditable=1]').each(function(){
        var old = $(this).text();
        $(this).editable('/pw/admin/JeditableProcess/', {
            submit      : 'OK',
            cancel      : 'Cancel',
            indicator   : 'Saving...',
            name        : 'content',
            onsubmit    : function(settings, original) {
                if (original.revert === $('input',this).val()) {
                    original.reset();
                    return false;
                }
            },
            submitdata  : {
                pid     : $(this).data('pid'),
                field   : $(this).data('field')
            },
            callback    : function(result, settings) {
                if (result.match(/<form id="ProcessLoginForm" class="InputfieldForm" method="post"/g)) {
                    var obj = { 'value': null, 'message': 'Permission denied! Not logged in!' };
                }
                else {
                    var obj = $.parseJSON(result);
                }
                
                if (obj.value === null) {
                    $(this).text(obj.message).delay(2000, 'sleep').queue('sleep', function(){ 
                        $(this).text(old);
                    }).dequeue("sleep");
                }
                else {
                    $(this).text(obj.value);
                }
            }
        });
    });
 });