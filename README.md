Simple proof of concept jEditable module to update single text fields

You have to add the admin / process page!
Title: JeditableProcess
Process: JeditableProcess

Example template file
~~~~
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
<?php 
    $jedit = $modules->get('Jeditable');
    $modules->get('JqueryCore');
    $jedit->scripts();
    
                foreach ($config->styles as $file) { echo "<link type='text/css' href='$file' rel='stylesheet' />\n"; }
                foreach ($config->scripts as $file) { echo "<script type='text/javascript' src='$file'></script>\n"; } 
                ?>
	</head>
	<body>
            <!-- additional element information by jeditable module (page id & field name)
            <h1 <?=$jedit->jeditable('title', $page)?>><?=$page->title?></h1>
            <div <?=$jedit->jeditable('body', $page)?>><?=$page->body?></div>
            <div <?=$jedit->jeditable('Textfield', $page)?>><?=$page->Textfield?></div>
	</body>
</html>
~~~~